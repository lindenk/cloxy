use clap::{App, Arg};
use hyper::{Server, Client};
use hyper::rt::{Future};
use itertools::Itertools;

use std::net::SocketAddr;

// Argument Validation
fn validate_ip(opt : String) -> Result<(), String> {
  let mut opt = opt.clone();
  if !opt.contains(":") {
    opt.push_str(":80");
  }

  let res = opt.parse::<SocketAddr>();

  if let Ok(_) = res {
    Ok(())
  } else {
    Err(String::from("Invalid IP address/port combination"))
  }
}

fn validate_route(opt : String) -> Result<(), String> {
  let opts : Vec<_> = opt.split(",").collect();
  if opts.len() != 2 {
    return Err(String::from("Route takes an endpoint and ip address like so: /path/to/resource,192.168.0.100:8091"))
  }

  //let (_, _) = opts.iter().next_tuple().unwrap();

  Ok(())
}

// Network structs
#[derive(Clone)]
struct ProxyRoute {
  endpoint : String,
  route : String
}

fn main() {
  let opts = App::new("Cloxy")
    .version("0.1")
    .about("Cloxy is a cli http proxy used for prototyping and development of web apps.")
    .author("Linden F. Krouse")
    .arg(Arg::with_name("interface")
      .short("i")
      .help("Interface(s) to host the proxy on.")
      .multiple(true)
      .takes_value(true)
      .validator(validate_ip)
      .default_value("0.0.0.0:80"))
    .arg(Arg::with_name("defaultroute")
      .short("d")
      .help("Default route to send traffic not matching a pattern.")
      .takes_value(true)
      .default_value("127.0.0.1:80"))
    .arg(Arg::with_name("route")
      .short("r")
      .help("Endpoint to match and it's associated route to proxy")
      .long_help("Example: Route /api to 192.168.0.100 port 8080 
                    -r /api,192.168.0.100:8080")
      .multiple(true)
      .takes_value(true)
      .validator(validate_route))
    .get_matches();
  
  // parse interfaces
  let interfaces : Vec<SocketAddr> = opts.values_of("interface").unwrap()
    .map(|x| x.parse().expect("Unable to parse interface"))
    .collect();
  
  // parse default route
  let default_route = String::from(opts.value_of("defaultroute").unwrap());

  // parse routes
  let routes : Vec<ProxyRoute> = opts.values_of("route").unwrap_or_default()
    .map(|r| {
      let (endpoint, route) = r.split(",").next_tuple().unwrap();

      //let mut route_string = String::from(route);
      /*if !route.contains(":") {
        route_string.push_str(":80");
        route = route_string.as_str();
      }*/

      ProxyRoute{
        endpoint: String::from(endpoint),
        route: route.parse().unwrap()
      }
    })
    .collect();
  
  // Host the proxy
  let server = Server::bind(&interfaces[0])
    .serve(move || {
      let client = Client::new();

      let routes = routes.clone();
      let default_route = default_route.clone();

      hyper::service::service_fn(move |mut req| {

        // check which route we go with and format it as a uri
        let mut url = default_route.clone();
        url = format!("{}{}", url, req.uri());
        for route in &routes {
          let url_start = req.uri().path_and_query().map(|x| x.as_str()).unwrap_or(req.uri().path());
          if url_start.starts_with(route.endpoint.as_str()) {
            url = format!("{}{}",
              route.route,
              url_start.replacen(route.endpoint.as_str(), "", 1));
          }
        }

        if !url.starts_with("http://") && !url.starts_with("https://") {
          url = format!("http://{}", url);
        }

        println!("Routing to {}", url);
        *req.uri_mut() = url.parse().unwrap();
        client.request(req)
      })
    })
    .map_err(|e| eprintln!("{}", e));

  hyper::rt::run(server);
}
